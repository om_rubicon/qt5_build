# How to Build Qt 6 for OsClient

Requirements: Linux, Docker

## Building Qt 6

If you haven't downloaded Qt yet, do so now. Orderman currently uses branch `v6.3.1-om`.


```
./get_qt6 [branch-or-tag]
```


And then the build itself:

```
./build_qt6
```

This creates a `qt6_om_sdk` in the current directory. It needs to be moved to `/opt/qt6_om_sdk` in order to work correctly.

## Replacing Qt6 Binaries in OsClient APK as per LGPL

After you have moved the Qt 6 SDK into `/opt/qt6_om_sdk`, you can use the `swap_qt6` script to replace Qt files covered
under the GNU LGPL license inside the OsClient application.

Requirements: Bash, zip/unzip, sed, awk

* First unpack the .om7app file (which is a ZIP archive) and extract the application (which is named like app-com.ncr.orderman.osclient.zip).
* extract the "app-*.zip" file and get the com.ncr.orderman.osclient.apk file from it (look under `data/app`).
* Run the swap script to replace the internal libs with your freshly built versoin: `./swap_qt6 com.ncr.orderman.osclient.apk`
* Sign the modified APK using [apksigner](https://developer.android.com/studio/command-line/apksigner).
* To install this modified APK on any Orderman sevice, you also have to sign it via the Orderman Signing service.

## Using the Qt 6 SDK in Qt Creator

Go to `Tools` -> `Options...` and choose `Kits`. Then, under the `Qt Versions` tab click the `Add...` button and navigate to
`/opt/qt6_om_sdk/bin/qmake`.

Now we need to create a kit using this Qt version. Switch to the `Kits` tab and click `Add`. Give the new kit a name of your choice
and select these options:

* Device type: Android Device
* Compiler - C: Android GCC (arm)
* Compiler C++: Android GCC (arm)
* Debugger: Android Debugger for Android GCC (arm)
* Qt version: _however you named the version in the previous step_

Once done, configure your Qt project for building using this kit via the `Projects` screen.

